<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Данные пациента</title>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9;
        }
    </style>
</head>
<body>
<h1>Данные пациента</h1>
<table class="tg">
    <tr>
        <th whidth="80">ID</th>
        <th whidth="120">First Name</th>
        <th whidth="120">Middle Name</th>
        <th whidth="120">Last Name</th>
        <th whidth="80">Age</th>
        <th whidth="120">Receipt Date</th>
    </tr>
        <tr>
            <th>${patient.id}</th>
            <th>${patient.firstName}</th>
            <th>${patient.middleName}</th>
            <th>${patient.lastName}</th>
            <th>${patient.age}</th>
            <th>${patient.receiptDate}</th>
        </tr>
</table>
</body>
</html>
