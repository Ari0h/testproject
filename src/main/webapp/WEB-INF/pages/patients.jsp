<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9;
        }
    </style>
</head>
<body>
<a href="../../index.jsp">Back</a>

<br/>
<br/>

<h1>Список пациентов</h1>

<c:if test="${!empty listPatients}">
    <c:set var="pageListHolder" value="${listPatients}" scope="session" />
    <table class="tg">
        <tr>
            <th whidth="80">ID</th>
            <th whidth="120">First Name</th>
            <th whidth="120">Middle Name</th>
            <th whidth="120">Last Name</th>
            <th whidth="80">Age</th>
            <th whidth="120">Receipt Date</th>
            <th whidth="60">Edit</th>
            <th whidth="60">Delete</th>
        </tr>
        <c:forEach var="patient" items="${pageListHolder.pageList}" >
            <tr>
                <th>${patient.id}</th>
                <th><a href="/data/${patient.id}" target="_blank">${patient.firstName}</a></th>
                <th>${patient.middleName}</th>
                <th>${patient.lastName}</th>
                <th>${patient.age}</th>
                <th>${patient.receiptDate}</th>
                <th><a href="/edit/${patient.id}">Edit</a></th>
                <th><a href="/delete/${patient.id}">Delete</a></th>
            </tr>
        </c:forEach>
    </table>
    <span style="float:left;">
    <c:choose>
        <c:when test="${pageListHolder.firstPage}">Prev</c:when>
        <c:otherwise><a href="/patients/prev">Prev</a></c:otherwise>
    </c:choose>
    </span>
    <span>
    <c:forEach begin="0" end="${pageListHolder.pageCount-1}" varStatus="loop">
        &nbsp;&nbsp;
        <c:choose>
            <c:when test="${loop.index == pageListHolder.page}">${loop.index+1}</c:when>
            <c:otherwise><a href="/patients/${loop.index}">${loop.index+1}</a></c:otherwise>
        </c:choose>
        &nbsp;&nbsp;
    </c:forEach>
    </span>
    <span>
    <c:choose>
        <c:when test="${pageListHolder.lastPage}">Next</c:when>
        <c:otherwise><a href="/patients/next">Next</a></c:otherwise>
    </c:choose>
    </span>
</c:if>

<br/>
<br/>

<h1>Добавить пациента</h1>
<c:url var="addAction" value="/patients/add"/>

<form:form action="${addAction}" modelAttribute="patient">
    <table>
        <c:if test="${!empty patient.firstName}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="firstName">
                    <spring:message text="First Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="firstName"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="middleName">
                    <spring:message text="Middle Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="middleName"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="lastName">
                    <spring:message text="Last Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="lastName"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="age">
                    <spring:message text="Age"/>
                </form:label>
            </td>
            <td>
                <form:input path="age"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="receiptDate">
                    <spring:message text="Receipt Date"/>
                </form:label>
            </td>
            <td>
                <form:input path="receiptDate"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <c:if test="${!empty patient.firstName}">
                    <input type="submit" value="<spring:message text="Edit"/>"/>
                </c:if>
                <c:if test="${empty patient.firstName}">
                    <input type="submit" value="<spring:message text="Add"/>"/>
                </c:if>
            </td>
        </tr>
    </table>


</form:form>

</body>
</html>
