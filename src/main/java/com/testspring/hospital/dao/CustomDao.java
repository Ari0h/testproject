package com.testspring.hospital.dao;

import com.testspring.hospital.model.Hospital;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Repository
public class CustomDao {

    private Logger logger = Logger.getLogger(CustomDao.class);
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    public List<Hospital> getAll() {
        try  {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery("from Hospital");
            List<Hospital> list =  query.list();
            return list;
        } catch (Exception e) {
            logger.error(e);
            return Collections.emptyList(); // тут по хорошему нужно кидать свое исключение и перехватывать его уровнем выше
        }
    }

    public boolean addPatient(@NonNull Hospital patient) {
        try  {
            Session session = sessionFactory.getCurrentSession();
            session.persist(patient);
            return true;
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    public boolean editPatient(@NonNull Hospital patient) {
        try  {
            Session session = sessionFactory.getCurrentSession();
            session.update(patient);
            return true;
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    public boolean deletePatient(@NonNull int id) {
        try  {
            Session session = sessionFactory.getCurrentSession();
            Hospital patient = session.load(Hospital.class, new Integer(id));
            if (patient != null) {
                session.delete(patient);
            }
            return true;
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    public Hospital getPatientById(@NonNull int id) {
        try  {
            Session session = sessionFactory.getCurrentSession();
            Hospital patient = session.get(Hospital.class, new Integer(id));
            logger.info("Пациент получен");
            return patient;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

}


