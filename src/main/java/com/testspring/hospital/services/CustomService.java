package com.testspring.hospital.services;

import com.testspring.hospital.dao.CustomDao;
import com.testspring.hospital.model.Hospital;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomService {

    private CustomDao customDao;
    private Logger logger = Logger.getLogger(CustomService.class);


    public void setCustomDao(CustomDao customDao) {
        this.customDao = customDao;
    }

    @Transactional
    public List<Hospital> getAll() {
        return customDao.getAll();
    }

    @Transactional
    public void addPatient(Hospital hospital) {
        boolean result = customDao.addPatient(hospital);
        if (result) {
            logger.info("Пациент добавлен в базу");
        } else {
            logger.error("Ошибка при добавлении пациента");
        }
    }

    @Transactional
    public void editPatient(Hospital hospital) {
        boolean result = customDao.editPatient(hospital);
        if (result) {
            logger.info("Данные пациента обновлены");
        } else {
            logger.error("Ошибка при обновлении данных");
        }
    }

    @Transactional
    public void deletePatient(int id) {
        boolean result = customDao.deletePatient(id);
        if (result) {
            logger.info("Пациент удален");
        } else {
            logger.error("Ошибка при удалении");
        }
    }

    @Transactional
    public Hospital getPatientById(int id) {
        return customDao.getPatientById(id);
    }
}
