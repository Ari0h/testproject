package com.testspring.hospital.controllers;

import com.testspring.hospital.model.Hospital;
import com.testspring.hospital.services.CustomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
public class CustomController {

    private CustomService customService;

    @Autowired(required = true)
    @Qualifier(value = "customService")
    public void setCustomService(CustomService customService) {
        this.customService = customService;
    }

    @RequestMapping(value = {"patients/{type}", "patients"}, method = RequestMethod.GET)
    public ModelAndView getAll(@PathVariable Map<String, String> pathVariablesMap, HttpServletRequest request) {
        PagedListHolder<Hospital> pagedList = null;
        String type = pathVariablesMap.get("type");
        if (type == null) {
            List<Hospital> patients =  this.customService.getAll();
            pagedList = new PagedListHolder<>();
            pagedList.setSource(patients);
            pagedList.setPageSize(3);
            request.getSession().setAttribute("listPatients", pagedList);
        } else if ("next".equals(type)) {
            pagedList = (PagedListHolder<Hospital>) request.getSession().getAttribute("listPatients");
            pagedList.nextPage();
        } else if ("prev".equals(type)) {
            pagedList = (PagedListHolder<Hospital>) request.getSession().getAttribute("listPatients");
            pagedList.previousPage();
        } else {
            pagedList = (PagedListHolder<Hospital>) request.getSession().getAttribute("listPatients");
            int indexPage = Integer.parseInt(type);
            pagedList.setPage(indexPage);
        }
        ModelAndView mv = new ModelAndView("patients", "patient", new Hospital());
        return mv;
    }


    @RequestMapping(value = "/patients/add", method = RequestMethod.POST)
    public String addPatient(@ModelAttribute("patient") Hospital hospital) {
        if (hospital.getId() == 0) {
            this.customService.addPatient(hospital);
        } else {
            this.customService.editPatient(hospital);
        }
        return "redirect:/patients";
    }

    @RequestMapping("/delete/{id}")
    public String deletePatient(@PathVariable("id") int id) {
        this.customService.deletePatient(id);
        return "redirect:/patients";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView editPatient(@PathVariable("id") int id, HttpServletRequest request) {
        PagedListHolder<Hospital> pagedList = (PagedListHolder<Hospital>) request.getSession().getAttribute("listPatients");
        int page = pagedList.getPage();
        ModelAndView mv = new ModelAndView("patients", "patient", this.customService.getPatientById(id));
        return mv;
    }

    @RequestMapping("/data/{id}")
    public String patientData(@PathVariable("id") int id, Model model) {
        model.addAttribute("patient", this.customService.getPatientById(id));
        return "data";
    }


}
