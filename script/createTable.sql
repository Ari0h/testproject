-- Table: hospital

-- DROP TABLE hospital;

CREATE TABLE hospital
(
  id serial NOT NULL,
  first_name character(120) NOT NULL,
  middle_name character(120),
  last_name character(120) NOT NULL,
  age smallint,
  receipt_date timestamp without time zone NOT NULL,
  CONSTRAINT id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hospital
  OWNER TO postgres;

-- Index: first_name

-- DROP INDEX first_name;

CREATE INDEX first_name
  ON hospital
  USING btree
  (first_name COLLATE pg_catalog."default");